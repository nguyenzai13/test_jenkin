package com.example.instagrambackend.util;

import lombok.AllArgsConstructor;
import lombok.Getter;
import org.springframework.http.HttpStatus;

@Getter
@AllArgsConstructor
public enum ErrorCodeConst {
    BUSINESS_ERROR("business.error", HttpStatus.BAD_REQUEST.value(), "business.error"),
    VALIDATION_ERROR("validation.error", HttpStatus.BAD_REQUEST.value(), "validation.error"),
    INTERNAL_SERVER_ERROR("internal.server.error", HttpStatus.INTERNAL_SERVER_ERROR.value(), "internal.server.error"),
    DUPLICATE_ERROR("duplicate.error", HttpStatus.BAD_REQUEST.value(), "duplicate.error"),
    DATA_NOT_FOUND_ERROR("data.not.found.error", HttpStatus.NOT_FOUND.value(), "data.not.found.error"),
    UNAUTHORIZED("unauthorized", HttpStatus.UNAUTHORIZED.value(), "unauthorized"),
    PERMISSION_DENIED("permission.denied", HttpStatus.FORBIDDEN.value(), "permission.denied"),
    INVALID_INPUT("invalid.input", HttpStatus.BAD_REQUEST.value(), "invalid.input");

    private final String code;
    private final int httpCode;
    private String message;

    @Override
    public String toString(){
        return "ResponseStatus{" +
                "code='" + code + '\'' +
                "httpCode='" + httpCode + '\'' +
                '}';
    }

}
