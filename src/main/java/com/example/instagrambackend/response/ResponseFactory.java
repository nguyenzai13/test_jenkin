package com.example.instagrambackend.response;

/* Description: Define a success and failed method for response */

import brave.Tracer;
import com.example.instagrambackend.util.ErrorCodeConst;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.MessageSource;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.Locale;

@Service
@Slf4j
@RequiredArgsConstructor
public class ResponseFactory {

    /* Description: Help translate message to other language */
    private final MessageSource messageSource;

    /* Description:  Define locale of the */
    private final Locale defaultLocale;
    private final Tracer tracer;

    public <T> ResponseEntity success(T data) {
        ResponseWrapper dataRes = ResponseWrapper.createResponseWrapper(data);
        dataRes.setSuccess(true);
        return ResponseEntity.ok().body(dataRes);
    }

    public <T> ResponseEntity fail(T data, ErrorCodeConst code, String message) {
        if(message == null || message.isEmpty()){
            message = code.getMessage();
        }
        ResponseWrapper dataRes = ResponseWrapper.createResponseWrapper(data);

        String validMessage = message;
        try{
            validMessage = messageSource.getMessage(message, null, defaultLocale);
        }
        catch(Exception e){
            log.debug("Cannot translate message: {}, error: {}", message, e);
        }

        dataRes.setSuccess(false);
        dataRes.setErrorCode(code.getCode());
        dataRes.setTraceId(tracer.currentSpan().context().traceIdString());
        dataRes.setMessage(validMessage);
        return ResponseEntity.status(code.getHttpCode()).body(dataRes);
    }

}
