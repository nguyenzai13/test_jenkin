package com.example.instagrambackend.controller;

import com.example.instagrambackend.dto.TestReqDTO;
import com.example.instagrambackend.entity.UserEntity;
import com.example.instagrambackend.exception.InstagramException;
import com.example.instagrambackend.repository.UserRepository;
import com.example.instagrambackend.response.ResponseFactory;
import com.example.instagrambackend.response.ResponseWrapper;
import com.example.instagrambackend.util.ErrorCodeConst;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.swing.text.html.parser.Entity;
import java.util.List;

@Slf4j
@RestController
@RequestMapping("/test/v1")
@RequiredArgsConstructor
public class TestController {

    private final ResponseFactory responseFactory;
    private final UserRepository userRepository;

    @GetMapping("/get-request")
    public ResponseEntity<ResponseWrapper<String>> testGetRequest() throws Exception{
        String name = "kainguyen";
//        this.testException();
        return responseFactory.success(name);
    }

    @PostMapping("/foo")
    public ResponseEntity<ResponseWrapper<String>> testGetRequest(@RequestBody TestReqDTO testReqDTO) throws Exception{
        log.info("kai nguyen: " + testReqDTO.getData());
        List<UserEntity> userEntityList = userRepository.findAll();
        return responseFactory.success(userEntityList);
    }

    private void testException() throws Exception{
        throw new InstagramException(ErrorCodeConst.BUSINESS_ERROR, null);
    }
}
