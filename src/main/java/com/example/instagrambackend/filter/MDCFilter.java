package com.example.instagrambackend.filter;

import com.google.common.io.ByteSource;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.filter.OncePerRequestFilter;
import org.springframework.web.util.ContentCachingRequestWrapper;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletRequestWrapper;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.nio.charset.StandardCharsets;

@Slf4j
public class MDCFilter extends OncePerRequestFilter {

    @Override
    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain) throws ServletException, IOException {
        log.info("start !!!");
        try{
            filterChain.doFilter(request, response);
        }
        finally {
            ContentCachingRequestWrapper requestWrapper = (ContentCachingRequestWrapper) request;
            log.info(new String(requestWrapper.getContentAsByteArray()));
            log.info("ACCESS LOG");
        }
    }
}
