package com.example.instagrambackend.security;

import com.example.instagrambackend.exception.InstagramException;
import com.example.instagrambackend.response.ResponseFactory;
import com.example.instagrambackend.util.ErrorCodeConst;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.filter.OncePerRequestFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.Map;

@Slf4j
@NoArgsConstructor
@AllArgsConstructor
public class AuthenticationLogin extends OncePerRequestFilter {
    private ResponseFactory responseFactory;
    private ObjectMapper mapper;

    @Override
    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain) throws IOException {
        String username = request.getParameter("username");
        String password = request.getParameter("password");

        Authentication authenticationObj = new UsernamePasswordAuthenticationToken(username, password);
        SecurityContextHolder.getContext().setAuthentication(authenticationObj);
        try{
            filterChain.doFilter(request,response);
        }
        catch (Exception e){
            ResponseEntity responseEntity = responseFactory.fail(null, ErrorCodeConst.BUSINESS_ERROR, null);
            response.setStatus(responseEntity.getStatusCodeValue());
            response.setCharacterEncoding(StandardCharsets.UTF_8.name());
            response.setContentType(MediaType.APPLICATION_JSON_VALUE);
            mapper.writeValue(response.getOutputStream(), responseEntity.getBody());
        }
    }
}
