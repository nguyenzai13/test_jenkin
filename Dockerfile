FROM openjdk:11

EXPOSE 7074

WORKDIR /app
ADD ./target/*.jar /app/app.jar

ENTRYPOINT [ "java", "-jar" , "app.jar" ]